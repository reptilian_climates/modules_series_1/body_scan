

'''
	https://docs.python.org/3/library/unittest.html#module-unittest
'''

'''
	(cd status && python -m unittest ut_1_status.py)

	(cd status && python -m unittest *status.py)
'''

import pathlib
from os.path import dirname, join, normpath


import status_modules.structure_paths as structure_paths
structure_paths.add (structure_paths.find ())


def FIND_path_status (paths, path_END):
	for path in paths:
		SPLIT = path ["path"].split (path_END)
	
		if (len (SPLIT) == 2 and len (SPLIT [1]) == 0):
			return path 

	print ("path_END:", path_END)
	raise Exception ("path NOT FOUND")


import body_scan

import time
import unittest
class CONSISTENCY (unittest.TestCase):
	def test_1 (THIS):
		print ("test_1")

		import pathlib
		from os.path import dirname, join, normpath
		this_folder = pathlib.Path (__file__).parent.resolve ()
		stasis = normpath (join (this_folder, "stasis/1"))

		SCAN = body_scan.start (
			glob_string = stasis + '/**/*_health.py',
			module_paths = [
				normpath (join (stasis, "modules"))
			],
			relative_path = stasis
		)
		status = SCAN ["status"]
		paths = status ["paths"]
		
		import json
		print ("UT 1 status FOUND", json.dumps (status ["stats"], indent = 4))

		assert (len (paths) == 2)
		assert (status ["stats"]["checks"]["passes"] == 4)
		assert (status ["stats"]["checks"]["alarms"] == 1)
		
		path_1 = FIND_path_status (paths, "path_1_health.py")
		assert (type (path_1) == dict)
		
		path_2 = FIND_path_status (paths, "path_2_health.py")
		assert (type (path_2) == dict)